$(document).ready(function(){

    /*
     * If window has resized, close menu
     */
    $( window ).resize(function() {
        $('#landing_nav_responsive').hide();
    });


    /*
     * Toggle the navigation open or closed each click.
     */
    $('.responsive-toggle-btn').click(function(){
        $('#landing_nav_responsive').toggle();

        if($('#landing_nav_responsive').is(':visible')) {
            $('html,body').css({'overflow' : 'hidden'});
        } else {
            $('html,body').css({'overflow' : 'auto'});
        }

    });


    /*
     * If we have scrolled past the initial view of home page, then set navigation to fixed
     */
    $(document).scroll(function(e){

        var page_scroll = $(window).scrollTop();

        if($('#home_container').length == 0 || (page_scroll > $('#home_container').position().top)) {
            $('#fixed_menu_home').css({ position : 'fixed', top : '0', width : '99%', left : '0.5%' });
        } else {
            $('#fixed_menu_home').css({ position : 'relative', top : '100%', left : '0', width : '100%' });
        }

    });

});