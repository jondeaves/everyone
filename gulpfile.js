var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    autoprefix = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css');


/*
 * Minify js and output to public
 */
gulp.task('js', function () {
    return gulp.src('assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('public/js'));
});

/*
 * Compile LESS to minified css, and output to public
 */
gulp.task('less', function () {
    gulp.src('assets/less/*.less')
        .pipe(less())
        .pipe(autoprefix('last 2 version', 'ie 8', 'ie 9'))
        .pipe(concat('app.min.css'))
        .pipe(minifyCSS({keepBreaks:false}))
        .pipe(gulp.dest('public/css'));
});


/*
 * Copy front-end vendor components to public folder
 */
gulp.task('publish', function(){

    gulp.src('vendor/bower_components/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('public/js/vendor/'));

    gulp.src('vendor/bower_components/modernizr/modernizr.js')
        .pipe(gulp.dest('public/js/vendor/'));

    gulp.src('vendor/bower_components/bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest('public/css/vendor/'));

    gulp.src('vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest('public/js/vendor/'));

    gulp.src('vendor/bower_components/font-awesome/css/font-awesome.min.css')
        .pipe(gulp.dest('public/css/'));

    gulp.src('vendor/bower_components/font-awesome/fonts/*')
        .pipe(gulp.dest('public/fonts/'));

});


/*
 * Watch for changes of each gulp task
 */
gulp.task('watch', function () {
    gulp.watch('assets/js/*.js', ['js']);
    gulp.watch('assets/less/*.less', ['less']);
    gulp.watch('vendor/bower_components/*', ['publish']);
});